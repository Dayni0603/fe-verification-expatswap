Project Name
Image Capture App

Description
The Image Capture App is a web application that allows users to capture photos using their device's camera and submit them for further processing. It provides a simple and intuitive interface for capturing photos, previewing them, and submitting them for upload.

Features
* Camera access: Users can access their device's camera to capture photos.
* Front and back camera support: Users can switch between the front and back cameras.
* Photo preview: Users can preview the captured photo before submitting.
* Photo submission: Users can submit the captured photo for further processing.
* Error handling: Appropriate error messages are displayed in case of any issues during photo capture or submission.
* Responsive design: The app is designed to adapt to different screen sizes and devices.
Technologies Used
HTML5
CSS3
JavaScript
React.js
Prerequisites
Node.js (version 18.16.0)
npm (version 9.5.1)
Getting Started
Clone the repository:

git clone <https://github.com/Dayni0603/doc-Img-capture-fe>

check on develop branch

```

Navigate to the project directory:


cd doc-Img-capture-fe
```

```

Install the dependencies:



pnpm install
```



```

Start the development server:

pnpm run dev --port=3000
```

Open your web browser and visit <http://localhost:3000> to access the Image Capture App.

Usage
On the home screen, the camera preview will be displayed.
Click the "Capture" button to capture a photo.
If necessary, use the "Use Back Camera" button to switch to the back camera.
The captured photo will be displayed for preview.
Click the "Retake" button to capture a new photo, or click the "Submit" button to submit the captured photo.
If the submission is successful, a success message will be displayed. Otherwise, an error message will be shown.
Contributing
Contributions are welcome! If you find any issues or would like to suggest enhancements, please submit a pull request.

License
This project is licensed under the MIT License.

Acknowledgments
OpenAI for providing the GPT-3.5 model used in the chatbot assistant.
